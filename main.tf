resource "null_resource" "helloWorld" {
    provisioner "local-exec" {
        command = "echo hello world"
    }
}

resource "null_resource" "byeWorld" {
    provisioner "local-exec" {
        command = "echo bye world"
    }
}
